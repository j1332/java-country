# Screenshots

See the screenshots [here](./information/README.md).

# Dependences

- JDK 11
- Java 11
- spring boot
- thymeleaf

# Database configuration

Dans le fichier `application.properties` dans `src` > `main` > `resources`, modifier les éléments suivants si besoin:

`spring.datasource.url=` ici
`spring.datasource.username=` ici
`spring.datasource.password=` ici


# Utlisation

Front-end en HTML / CSS / JavaScript intégrer au projet.

Back-end :

```sh
# A la racine du projet
 ./mvnw clean package
 java -jar .\target\App-0.0.1-SNAPSHOT.jar
```

**Vous avez besoin d'une base de données prête à démarrer l'API**


# API

## URL de l'application

 http://localhost:8080/

## Requêtes API

- `GET /countryApi/showAllCountry` => Liste des pays
- `POST /countryApi/newCountry` => Ajouter un pays dans la BDD
- `PUT /countryApi/editCountry/{id}` => Modifier un pays avec son ID
- `DELETE /countryApi/deleteCountry/{id}` => Supprimer un pays avec son ID

## Requetes en ligne de commande (cURL)

*Dans le terminal, tapez: `curl`*

*Puis copier / coller:*

GET /showAllCountry 

```sh
http://localhost:8080/countryApi/showAllCountry
```

GET /{id}

```sh
http://localhost:8080/countryApi/{id}
```

POST /newCountry/

*Créer le pays avec un RANDOM prédéfinie à la récéption de la requetes*

```sh
-d '{}' -H "Content-Type: application/json" http://localhost:8080/countryApi/newCountry
```

PUT /editCountry/{id}

*Modifie le pays avec un RANDOM prédéfinie à la récéption de la requetes*

```sh
-d '{}' -H "Content-Type: application/json" -X PUT http://localhost:8080/countryApi/editCountry/4
```

DELETE /deleteCountry/{id}

```sh
-X DELETE http://localhost:8080/countryApi/deleteCountry/3
```

