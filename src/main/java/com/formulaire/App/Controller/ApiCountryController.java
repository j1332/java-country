package com.formulaire.App.Controller;

import java.util.List;
import java.util.Optional;
import java.util.Random;

import com.formulaire.App.Entity.CountryEntity;
import com.formulaire.App.Repository.CountryRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/countryApi")
public class ApiCountryController {

    @Autowired
    private CountryRepository countryRepository;

    // Liste des éléments pour le RANDOM
    private static final String[] NAMES = new String[] { "France", "Espagne", "Russie", "Japon", "Angleterre", "Allemagne" };

    // GET /countries => La liste de tous les pays
    @GetMapping(path = "/showAllCountry", produces = "application/json")
    public List<CountryEntity> showAllCountry() {

        List<CountryEntity> countryAll = this.countryRepository.findAll();

        return countryAll;
    }

    // GET /country/{id} => Les informations du pays portant cet id
    @GetMapping(path = "/{id}", produces = "application/json")
    public CountryEntity getCountry(@PathVariable Long id) {

        Optional<CountryEntity> countryExist = this.countryRepository.findById(id);
        return countryExist.isPresent() ? countryExist.get() : null;
    }

    // POST /country => Enregistre un pays
    @PostMapping(path = "/newCountry")
    public void newCountry() {

        Long empIdMax = this.countryRepository.getMaxId();
        long id = empIdMax + 1;

        CountryEntity country = new CountryEntity();

        // Créer le pays avec un RANDOM prédéfinie à la récéption de la requetes
        int random = new Random().nextInt(6);
        String fullName = NAMES[random];

        country.setId(id);
        country.setName(fullName);
        this.countryRepository.save(country);

        System.out.println("Create" + country);
    }

    // PUT /country/{id} => Met à jour un pays portant cet id
    @PutMapping(path = "/editCountry/{id}")
    public void updateCountry(@PathVariable long id) {

        // Modifie le pays avec un RANDOM prédéfinie à la récéption de la requetes
        int random = new Random().nextInt(6);
        String fullName = NAMES[random];

        Optional<CountryEntity> countryExist = this.countryRepository.findById(id);
        boolean exists = countryExist.isPresent();
        if (exists) {
            countryExist.get().setId(id);
            countryExist.get().setName(fullName);
            this.countryRepository.save(countryExist.get());
        }
        System.out.println("Edit" + countryExist);
    }

    // DELETE /country/{id} => Supprime le pays portant cet id
    @DeleteMapping("/deleteCountry/{id}")
    public void deleteCountry(@PathVariable Long id) {

        Optional<CountryEntity> countryExist = this.countryRepository.findById(id);
        boolean exists = countryExist.isPresent();
        if (exists) {
            this.countryRepository.delete(countryExist.get());
        }
        System.out.println("Delete");

    }
}
