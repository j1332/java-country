package com.formulaire.App.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class AppController {

    // DEPENDANCE THYMELEAF
    @GetMapping(value = { "/index", "/" })
    public String getIndex() {
        return "index";
    }
}
