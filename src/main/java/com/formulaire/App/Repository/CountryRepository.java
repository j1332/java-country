package com.formulaire.App.Repository;

import com.formulaire.App.Entity.CountryEntity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


public interface CountryRepository extends JpaRepository<CountryEntity, Long> { 

    CountryEntity findByName(String name);

    @Query("SELECT coalesce(max(c.id), 0) FROM CountryEntity c")
    Long getMaxId();

    

}
