

//////////////////////////////////////////////
//////////////       CRUD       ///////////////
///////////////////////////////////////////////


const Constants = {
    //COUNTRY CRUD URL
    urlCountryIndex: 'http://localhost:8080/countryApi/showAllCountry',
    urlCountryNew: 'http://localhost:8080/countryApi/newCountry',
    urlCountryShow: 'http://localhost:8080/countryApi/',
    urlCountryEdit: 'http://localhost:8080/countryApi/editCountry/',
    urlCountryDelete: 'http://localhost:8080/countryApi/deleteCountry/',
}


//////////////////////////////////
// ***** SHOW ALL COUNTRY **** //
////////////////////////////////

const onIndexCountry = () => {

    // Récupération de l'élement
    const countryListItem = document.getElementsByTagName("tbody")[0];
    countryListItem.innerHTML = "";

    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    var requestOptions = {
        method: 'GET',
        headers: myHeaders,
    };

    // Récupération des données
    fetch(Constants.urlCountryIndex, requestOptions)
        .then(response => response.json())
        .then(datas => datas.map((data, index) => {
            let countries = [];
            // console.log(data)

            // Ajout une interface pour chaque élément du tableau
            countries.push(`<tr>
                <td>${data.id}</td>
                <td>${data.name}</td>
                <td>
                    <button class="edit btn btn-sm btn-outline-success" value="${index}">Modifier</button>
                    <button class="delete btn btn-sm btn-outline-danger" value="${index}">Supprimer</button>
                </td>
                </tr>`);
            // console.log(countries);
            countryListItem.innerHTML += countries;

            // Chaque bouton "Editer"
            document.querySelectorAll("button.edit").forEach(b => {
                b.addEventListener("click", function () {
                    return btnEditCountry(datas, this.value);
                });
            });

            // Chaque bouton "Supprimer"
            document.querySelectorAll("button.delete").forEach(b => {
                b.addEventListener("click", function () {
                    return btnDeleteCountry(datas, this.value);
                });
            });

        }))
        // .then(datas => console.log(datas))
        // .then(function (datas) {
        //     for (let index = 0; index < datas.length; index++) {
        //         const element = datas[index];
        //         console.log(element)

        //         data += `<tr>
        //         <td>${element.id}</td>
        //         <td>${element.name}</td>
        //         </tr>`;

        //         // Affichage des éléments dans le HTML
        //         if (datas.length > 0) {
        //             // Affichage des données dans le tableau
        //             countryListItem.innerHTML += data;
        //         } else {
        //             // Aucune donnée
        //             countryListItem.innerHTML += "<p>Aucune ligne trouvée</p>";
        //         }
        //     }
        // })
        .catch(error => console.log('error', error));

}

/////////////////////////////
// ***** NEW COUNTRY **** //
///////////////////////////

// Créer le pays avec un RANDOM prédéfinie à la récéption de la requetes
const newCountry = () => {

    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    var raw = JSON.stringify({});

    var requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: raw,
        redirect: 'follow'
    };

    fetch(Constants.urlCountryNew, requestOptions)
        .then(response => response.text())
        .then(result => console.log(result))
        .catch(error => console.log('error', error));
}



//////////////////////////////
// ***** EDIT COUNTRY **** //
////////////////////////////

function btnEditCountry(datas, index) {
    // Récupération de la ligne via son index
    const country = datas.find((data, i) => {
        console.log(index)
        return i == index;
    });

    // Alimentation des champs
    document.getElementById("id").value = country.id;
    document.getElementById("name").value = country.name;
    document.getElementById("hidden").value = index;

    displayForm();
}

// Modifie le pays avec un RANDOM prédéfinie à la récéption de la requetes
function editCountry(idItem) {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    var raw = JSON.stringify({});

    var requestOptions = {
        method: 'PUT',
        headers: myHeaders,
        body: raw,
        redirect: 'follow'
    };

    fetch(Constants.urlCountryEdit + idItem, requestOptions)
        .then(response => response.text())
        .then(result => console.log(result))
        .catch(error => console.log('error', error));
}


////////////////////////////////
// ***** DELETE COUNTRY **** //
//////////////////////////////

function btnDeleteCountry(datas, index) {
    if (confirm("Confirmez-vous la suppression de cette Ville ?")) {
        // Récupération de la ligne via son index
        const country = datas.find((data, i) => {
            console.log(index)
            return i == index;
        });
        deleteCountry(country.id);
    }
    return onIndexCountry();
}

function deleteCountry(idItem) {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    var raw = JSON.stringify({});

    var requestOptions = {
        method: 'DELETE',
        headers: myHeaders,
        body: raw,
        redirect: 'follow'
    };

    fetch(Constants.urlCountryDelete + idItem, requestOptions)
        .then(response => response.text())
        .then(result => console.log(result))
        .catch(error => console.log('error', error));
}

//////////////////////////////////////////////
//////////////     FUNCTION     ///////////////
///////////////////////////////////////////////


console.table(onIndexCountry())

// Ajoute un eventListener lors de la saisie de texte 
// dans ce champs de recherche.
// document
//     .querySelectorAll("input[type=search]")[0]
//     .addEventListener("input", search);

// function search() {
//     console.log(this.value);
//     const filteredData = countryListItem.filter(c =>
//         c.name.toLowerCase().includes(this.value.toLowerCase())
//     );
//     onIndexCountry(filteredData);
// }

// Cache le formulaire
const elForm = document.getElementById("form");
elForm.style.display = "none";
const elContent = document.getElementById("content");

// Ajoute un eventListerner au clic sur le bouton d'ajout
document.getElementById("form-add").addEventListener("click", function () {
    displayForm();
});

// Ajoute un eventListerner au clic sur le bouton Annuler
document.getElementById("form-cancel").addEventListener("click", function () {
    hideForm();
});

// Appel la fonction displayForm pour AFFICHER le formulaire 
// et MASQUER le reste du contenu
function displayForm() {
    elForm.style.display = "block";
    elContent.style.display = "none";
}

// Appel la fonction hideForm pour MASQUER le formulaire 
// et AFFICHER le reste du contenu
function hideForm() {
    elForm.style.display = "none";
    elContent.style.display = "block";

    document.getElementById("id").value = "";
    document.getElementById("name").value = "";
    document.getElementById("hidden").value = "";
}

// Ajoute un eventListerner au clic sur le bouton d'enregistrement du formulaire
document.getElementById("form-save").addEventListener("click", function () {
    // Récupération des champs
    const id = document.getElementById("id").value;
    const name = document.getElementById("name").value;

    if (id && name) {
        // Nouvelle ligne
        const Country = { id: id, name: name };

        // Ajout de la nouvelle ville ou modification
        if (document.getElementById("hidden").value === "") {
            console.log(document.getElementById("hidden").value.lenght)
            newCountry();
            //  movies.splice(document.getElementById("hidden").value, 1, movie);
        } else {
            console.log(document.getElementById("hidden").value)
            editCountry(document.getElementById("id").value)
        }

        // Affichage du nouveau tableau
        return onIndexCountry();
    }
});