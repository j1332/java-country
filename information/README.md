# Screenshots

### **FRONTEND**

Liste des Pays avec un bouton pour créer

![GET All](./resources/FrontList.jpg)

Formulaire de création (*élément input non envoyé mais doit être compléter pour valider*)

![GET NEW](./resources/Form.jpg)

Formulaire de modification utilisant le même formulaire de création (*élément input non envoyé mais doit être compléter pour valider*)

![GET EDIT](./resources/Form-PUT.jpg)

### **cURL**

GET /showAllCountry

![cURL GET](./resources/Curl-getAll.jpg)
